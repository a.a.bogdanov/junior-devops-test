FROM python:3.8-alpine
COPY requirements/production.txt /tmp/requirements/production.txt
RUN addgroup -g 2000 app \
    && adduser -u 2000 -G app -s /bin/sh -D app
USER app
RUN pip install --no-cache -r tmp/requirements/production.txt --no-warn-script-location

COPY dist /tmp/dist
RUN pip install /tmp/dist/* --no-warn-script-location

EXPOSE 8085
ENTRYPOINT ["python3"]
CMD ["run_service.py", "--port", "8085"]
